var kue = require('kue'),
  queue = kue.createQueue(),
  Q = require('q'),
  _ = require('lodash');

var express = require('express');
var ui = require('kue-ui');
var cors = require('cors');

function getEmail(){
	return {
    	title: 'NOT COOL ' + Math.random()
	  , to: 'test_' + Math.random() + '@learnboost.com'
	  , template: 'welcome-email',
     cool: Math.random() > 0.2 ? true : false
	};
}

queue.process('email', function(job, done){
  	if (!job.data.cool) {
  		job.errorData = {foo: 13, bar:19};
  		return done(new Error('not cool error'), job.data);
  	}
  	else {
  		//console.log("EMAIL TO: " + job.data.to);
  		job.data.wicked = 13;

  		done();
  	}
});

 function setupJob(type, data, delay, priority) {
	var job = queue.create(type, data);
    console.log("JOB ID: " + job.id);
	job.on('complete', function(result){
	  //console.log('Job completed with data ' + result.to);

	}).on('failed attempt', function(errorMessage, doneAttempts){
	  console.log('Job failed');

	}).on('failed', function(errorMessage){
	  //console.log("ERROR: " + errorMessage + " ID: " + job.id);


	}).on('progress', function(progress, pdata){

	  console.log('\r  job #' + job.id + ' ' + progress + '% complete with data ', pdata );

	}).on('promotion', function () {
    	console.log('renewal job promoted :' + job.id);
	});
	if (priority)
  	 	job.priority(priority);
	if (delay){
		var now = new Date();
		job.data.delayedUntil = new Date(now.getTime() + delay*20000);
		job.delay(job.data.delayedUntil);
		job.priority('high');
	}

  	//job.removeOnComplete(true);
	job.save();

	return job;
 }
function getStats(que) {
	var deferred = Q.defer();
	var info = {
		activeCount: 0,
		completeCount: 0,
		failedCount: 0,
		inactiveCount: 0,
		delayedCount: 0
	};
	que.completeCount(function(err, total){
		if (err) deferred.reject(err);
		info.completeCount = total;
		que.activeCount(function(err, total){
			info.activeCount = total;
			que.delayedCount(function(err, total){
			info.delayedCount = total;
			deferred.resolve(info);
			})
		})
	});

	return deferred.promise;
}
 var jobs = [];
 var statsInterval;
 console.log("STARTING UP");
 //console.log("Q functions " , _.functions(queue));
 var tDelay = 10;
 var priorities = ["low","normal","medium","high","critical"];
 var currPriority = "high";

function clearQueue(states, n){
	n = n || 1000;
	_.each(states, function(state, k){
		//console.log("CLEARING state: [" + state + "]");
	kue.Job.rangeByState( state, 0, n, 'asc', function( err, jobs ) {
	  jobs.forEach( function( job ) {
	    job.remove( function(){
	      //console.log( "del " + job.id );
	    });
	  })
		});

	})

}


function loadJobs(){
  var dInput = 1;
 for(var i=0;i<10;i++){
 	if (i % 3 === 0 && i>0){
 		tDelay = dInput++;
 	}
 	else {
 		tDelay = false;
 	}
 	jobs.push(setupJob('email',getEmail(), tDelay, priorities[i % priorities.length]));
 }
}


// start the UI
ui.setup({
    apiURL: '/api',
    baseURL: '/test',
    updateInterval: 500
});

var app = express();
app.use(cors());
app.use('/api', kue.app);
app.use('/test', ui.app);


clearQueue([ 'complete', 'failed','delayed','inactive'], 100);
//clearQueue(['complete', 'failed','delayed','delayed','complete','complete','complete']);
setTimeout(loadJobs, 1000);

app.listen(13913);
//queue.promote();

//setInterval(loadJobs, 60000);